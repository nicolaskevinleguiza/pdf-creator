import app from './config/express'

function App () {
  app.listen(app.get('port'), () => {
    console.info('Server listen on port:', app.get('port'));
  })
}

export default App