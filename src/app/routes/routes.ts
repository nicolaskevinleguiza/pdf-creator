import {Router} from 'express'

import rootRouter from "./root.route";
import imagesRouter from './images.route';

const routes = Router()

routes.use(rootRouter)
routes.use(imagesRouter)

export default routes 