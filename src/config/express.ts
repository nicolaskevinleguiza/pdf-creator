import express from "express";
import { config } from 'dotenv'
import path from "path";
import routes from '../app/routes/routes'

config()

const app = express()
app.set('port', process.env.PORT || 3000)

app.use(express.static(path.join(__dirname, '../static')))
app.use(routes)

export default app