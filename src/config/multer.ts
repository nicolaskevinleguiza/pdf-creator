import multer from "multer";
import path from "path";

function getUploader (): multer.Multer {
  const storage = multer.diskStorage({
    destination: (req, file, callback) => {
      
      const destinations: Record<string, string> = {
        "image/png": "imgs",
        "image/jpg": "imgs",
        "image/jpeg": "imgs",
        "application/pdf": "pdfs"
      }
      
      const folder: string = destinations[file.mimetype] ?? 'unknow'
      callback(null, path.join(__dirname, `../static/${folder}`))
    },
    filename: (req, file, callback) => {
      callback(null, file.originalname)
    }
  })
  const uploader = multer({ storage: storage })
  return uploader
}

export default getUploader