import { Router, Request, Response } from "express";
import getUploader from "../../config/multer";
import createPdf from "../controllers/images.controller";

const uploader = getUploader()

const imagesRouter = Router()
const endPoint = '/images'

imagesRouter.get(endPoint, (req: Request, res: Response) => {
  res.status(200).send('Images endpoint')
})

imagesRouter.post( endPoint, uploader.any(), createPdf)

export default imagesRouter