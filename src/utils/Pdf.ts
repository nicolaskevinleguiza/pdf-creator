import { PDFDocument, PDFPage, rgb } from 'pdf-lib'
import imageSize from 'image-size'
import fs from 'fs'

class Pdf {

  #pdfDoc: PDFDocument | null = null
  #pages: PDFPage[] | null = null

  constructor() {}

  public async open (pdfPath: string): Promise<boolean> {
    try {
      const pdfBytes = fs.readFileSync(pdfPath)
      this.#pdfDoc = await PDFDocument.load(pdfBytes)
      this.#pages = this.#pdfDoc.getPages()
      return true
    } catch (err) {
      console.error('File cannot be opened, error:', err);
      return false
    }
  }

  public hiddeTogglLogo (): void {
    if (this.#pages) {
      for (const page of this.#pages) {
        const { width, height } = page.getSize()
        page.drawRectangle({
          color: rgb(1, 1, 1),
          width: 100,
          height: 50,
          x: width - 120,
          y: height - 70
        })
      }
    } else {
      console.error('Error: Pages is null');
    }
  }

  private getImagesSize (imagePath: string, perfectWidth: number): { imageWidth: number, imageHeight: number } {
    const { width: imageWidth, height: imageHeight } = imageSize(imagePath)
    if (imageWidth && imageHeight) {
      const newHeight = (imageHeight / imageWidth) * perfectWidth
      return { imageWidth: perfectWidth, imageHeight: newHeight }
    } else {
      console.error('Cannot get image size of "', imagePath,'"');
      return { imageWidth: 100,  imageHeight: 50 }
    }
  }

  public async placeImage (
    pageIndex: number,
    imagePath: string,
    ...opts: any[]
  ) {
    if (this.#pages) {
      const { marginTop, marginRight } = opts[0] ?? { marginTop: null, marginRight: null }
      
      const { imageWidth, imageHeight } = this.getImagesSize(imagePath, 100)
      const { width: pdfWidth, height: pdfHeight } = this.#pages[pageIndex].getSize()
      const imageSource = fs.readFileSync(imagePath)
      const image = await this.#pdfDoc?.embedJpg(imageSource)
      if (image)
        this.#pages[pageIndex].drawImage(image, {
          width: imageWidth,
          height: imageHeight,
          x: pdfWidth - (imageWidth + marginRight ?? 30),
          y: pdfHeight - (imageHeight + marginTop ?? 30)
        })
      else
        console.error('Error: Image not found');
    }
  }

  public async save (outPath: string): Promise<Boolean> {
    const pdfBytes = await this.#pdfDoc?.save()
    if (pdfBytes) {
      fs.writeFileSync(outPath, pdfBytes)
      return true
    }
    else {
      console.error('Error: Cannot save pdf');
      return false
    }
  }
}

export default Pdf