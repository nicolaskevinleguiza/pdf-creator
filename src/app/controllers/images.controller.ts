import { Request, Response } from "express";
import path from "path";
import Pdf from '../../utils/Pdf'

async function createPdf (req: Request, res: Response): Promise<void> {
  const files: Express.Multer.File[] = req.files as Express.Multer.File[] ?? []
  const filesMap = new Map()

  for (const file of files) {
    if (file.fieldname === 'pdf')
      filesMap.set('pdf-name', file.originalname)
    filesMap.set(file.fieldname, file.path)
  }
  
  const pdf = new Pdf()
  if (!await pdf.open(filesMap.get('pdf')))
    res.status(500).send({ error: `Cannot open files: ${filesMap.get('pdf')}` })

  pdf.hiddeTogglLogo()
  await pdf.placeImage(0, filesMap.get('mainImage'))
  await pdf.placeImage(1, filesMap.get('secondaryImage'))

  const processedPdf = `../../static/out/${filesMap.get('pdf-name')}`

  await pdf.save(path.join(__dirname, processedPdf))
  
  res.status(200).sendFile(path.join(__dirname, processedPdf))
}

export default createPdf